#ifndef _NTH_APPLICATION_INFO_
#define _NTH_APPLICATION_INFO_

#define NTH_VERSION "a0-2017-04-27"

#define _BSD_SOURCE
#define _GNU_SOURCE

#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>

#include "abuf.h"

#include "highlighting.h"

typedef struct erow {
    int idx;
    int size;
    int rsize;
    char *chars;
    char *render;
    unsigned char *hl;
    int hl_openComment;
} erow;

typedef struct {
    int x;
    int y;
} coordinates;

typedef struct {
    coordinates cursor;
    coordinates scrSize;
    coordinates scrPos;
    coordinates markPos;
    int tabWidth;
    int numrows;
    int rawmode;
    erow *row;
    int dirty;
    char *filename;
    char statusmsg[80];
    abuf *inputCommand;
    time_t statusmsg_time;
    editorSyntax *syntax;
} editorConfig;

static editorConfig edState;

enum KEY_ACTION {
        KEY_NULL = 0,
        CTRL_A,
        CTRL_B,
        CTRL_C,
        CTRL_D,
        CTRL_E,
        CTRL_F,
        CTRL_G,
        CTRL_H,
        TAB,
        CTRL_J,
        CTRL_K,
        CTRL_L,
        ENTER,
        CTRL_N,
        CTRL_O,
        CTRL_P,
        CTRL_Q,
        CTRL_R,
        CTRL_S,
        CTRL_T,
        CTRL_U,
        CTRL_V,
        CTRL_W,
        CTRL_X,
        CTRL_Y,
        CTRL_Z,
        ESC,
        CTRL_SLASH=31,
        BACKSPACE =  127,
        /* Not ASCII.  Arbitrary nums we use internally. */
        ARROW_LEFT = 1000,
        ARROW_RIGHT,
        ARROW_UP,
        ARROW_DOWN,
        FUNCTION_1,
        FUNCTION_2,
        FUNCTION_3,
        SHIFT_TAB,
        META_TAB,
        CTRL_META_SPACE,
        META_B,
        META_F,
        META_G,
        META_R,
        META_T,
        META_W,
        META_DOT,
        META_COMMA,
        DEL_KEY,
        END_KEY,
        HOME_KEY,
        META_BANG,
        META_CROCK,
        META_ENTER,
        META_GATOR,
        META_OPEN_BRACE,
        META_OPEN_BRACKET,
        META_OPEN_PAREN,
        META_PIPE,
        META_CTRL_A,
        META_CTRL_B,
        META_CTRL_N,
        META_CTRL_P,
        PAGE_DOWN,
        PAGE_UP
};

typedef int (*command_processor)(int, coordinates);
typedef void (*command_special)(void);

void set_status(const char *fmt, ...);

#define NTH_QUIT_TIMES 3
#define STATUS_BAR_COUNT 2
#define NTH_DEFAULT_TAB_WIDTH 4
#define NTH_MAX_TAB_WIDTH 40
#define NTH_COMMAND_BAD 0
#define NTH_COMMAND_OK 1
#define NTH_COMMAND_ABORT -1
#define NTH_COMMAND_ONGOING 0
#define NTH_COMMAND_FINISHED 1

coordinates doc_position_of_cursor(const editorConfig);
void redraw_region(coordinates beginning, coordinates ending);
int push_cursor_with(const int key);
void move_cursor(int key);
void refresh_screen(void);
void center_screen_upon_cursor(void);

#define min(x,y) (x > y) ? y : x;
#define max(x,y) (x < y) ? y : x;

#define CTRLSEQ_HIDE_CURSOR "\x1b[?25l"
#define CTRLSEQ_GO_HOME "\x1b[H"
#define CTRLSEQ_CLEAR_TO_EOL "\x1b[0K"
#define CTRLSEQ_CLEAR_SCREEN "\x1b[2J"
#define CTRLSEQ_SHOW_CURSOR "\x1b[?25h"
#define CTRLSEQ_GET_CURSOR_POS "\x1b[6n"

#endif
