#ifndef _ATTO_SYNTAX_HIGHLIGHTING_
#define _ATTO_SYNTAX_HIGHLIGHTING_

#define HL_NORMAL 0
#define HL_NONPRINT 1
#define HL_COMMENT 2
#define HL_MLCOMMENT 3
#define HL_KEYWORD1 4
#define HL_KEYWORD2 5
#define HL_STRING 6
#define HL_NUMBER 7
#define HL_MATCH 8

#define CTRLSEQ_FG_BLACK "\x1b[30m"
#define CTRLSEQ_FG_RED "\x1b[31m"
#define CTRLSEQ_FG_GREEN "\x1b[32m"
#define CTRLSEQ_FG_YELLOW "\x1b[33m"
#define CTRLSEQ_FG_BLUE "\x1b[34m"
#define CTRLSEQ_FG_MAGENTA "\x1b[35m"
#define CTRLSEQ_FG_CYAN "\x1b[36m"
#define CTRLSEQ_FG_WHITE "\x1b[37m"
#define CTRLSEQ_FG_DEFAULT "\x1b[39m"
#define CTRLSEQ_COLOR_INVERT "\x1b[7m"
#define CTRLSEQ_COLOR_RESET "\x1b[0m"
#define CTRLSEQ_TEXT_BOLD "\x1b[1m"
#define CTRLSEQ_TEXT_DIM "\x1b[2m"
#define CTRLSEQ_TEXT_BLINK "\x1b[5m"

#define HL_HIGHLIGHT_STRINGS (1<<0)
#define HL_HIGHLIGHT_NUMBERS (1<<1)

typedef struct {
    char **filematch;
    char **keywords;
    char singleline_comment_start[2];
    char multiline_comment_start[3];
    char multiline_comment_end[3];
    int flags;
} editorSyntax;

typedef struct hlcolor {
    int r,g,b;
} hlcolor;

/* =========================== Syntax highlights DB =========================
 *
 * In order to add a new syntax, define two arrays with a list of file name
 * matches and keywords. The file name matches are used in order to match
 * a given syntax with a given file name: if a match pattern starts with a
 * dot, it is matched as the last past of the filename, for example ".c".
 * Otherwise the pattern is just searched inside the filenme, like "Makefile").
 *
 * The list of keywords to highlight is just a list of words, however if they
 * a trailing '|' character is added at the end, they are highlighted in
 * a different color, so that you can have two different sets of keywords.
 *
 * Finally add a stanza in the HLDB global variable with two two arrays
 * of strings, and a set of flags in order to enable highlighting of
 * comments and numbers.
 *
 * The characters for single and multi line comments must be exactly two
 * and must be provided as well (see the C language example).
 *
 * There is no support to highlight patterns currently. */

/* C / C++ */
char *C_HL_extensions[] = {".c",".cpp",".C",".cxx",".cc",".h", ".hpp", NULL};
char *C_HL_keywords[] = {
        /* A few C / C++ keywords */
        "switch","if","while","for","break","continue","return","else",
        "struct","union","typedef","static","enum","class", "goto", "do",
        /* C types */
        "int|","long|","double|","float|","char|","unsigned|","signed|",
        "void|","const|",NULL
};

char *JS_HL_extensions[] = {".js",".javascript",".ecmascript",".es6", NULL};
char *JS_HL_keywords[] = {
        /* JS keywords */
        "switch","if","while","for","break","continue","return","else",
        "function","typedef","static","enum","class","do","Math",
        "var","JSON","console","process","window","null","undefined",
        "module","const","require",NULL
};

/* Here we define an array of syntax highlights by extensions, keywords,
 * comments delimiters and flags. */
editorSyntax HLDB[] = {
    {
        C_HL_extensions,
        C_HL_keywords,
        "//","/*","*/",
        HL_HIGHLIGHT_STRINGS | HL_HIGHLIGHT_NUMBERS
    },
    {
        JS_HL_extensions,
        JS_HL_keywords,
        "//","/*","*/",
        HL_HIGHLIGHT_STRINGS | HL_HIGHLIGHT_NUMBERS
    }
};

#define HLDB_ENTRIES (sizeof(HLDB)/sizeof(HLDB[0]))

#endif
