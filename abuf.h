#ifndef _NTH_APPEND_BUFFER_HEADER_
#define _NTH_APPEND_BUFFER_HEADER_

#include <stdlib.h>
#include <string.h>

typedef struct {
    char *b;
    int len;
    int size;
} abuf;

abuf *ab_init(int initialSize);
void ab_destroy(abuf *ab);
void ab_append(abuf *ab, const char *s, int len);
void ab_attach(abuf *ab, const int character);
void ab_concat(abuf *ab, const char *s);
char *ab_content(abuf *ab);
int ab_length(abuf *ab);
void ab_flush(abuf *ab);
void ab_rub_out(abuf *ab);

#endif
