#include "abuf.h"

abuf *ab_init(int initialSize) {
    abuf *newGuy = calloc(1, sizeof(abuf));
    newGuy->size=initialSize;
    newGuy->b=calloc(initialSize, sizeof(char));
    newGuy->len=0;
    return newGuy;
}

void ab_resize(abuf *ab, const int desiredSize) {
    char *new;
    if(!ab || ab->size>desiredSize+1)
        return;
    ab->size+=desiredSize+1;
    new = realloc(ab->b, ab->size);
    if(new == NULL)
        exit(3);
    ab->b=new;
}

void ab_append(abuf *ab, const char *s, int len) {
    ab_resize(ab, ab->len+len);
    memcpy(ab->b+ab->len, s, len);
    ab->len+=len;
}

void ab_attach(abuf *ab, const int character) {
    ab_resize(ab, ab->len+1);
    ab->b[ab->len++]=character;
}

void ab_concat(abuf *ab, const char *s) {
    if(!s)
        return;
    ab_append(ab, s, strlen(s));
}

void ab_destroy(abuf *ab) {
    if(!ab)
        return;
    if(ab->b)
        free(ab->b);
    free(ab);
}

char *ab_content(abuf *ab) {
    if(!ab)
        return NULL;
    return ab->b;
}

int ab_length(abuf *ab) {
    if(!ab)
        return 0;
    return ab->len;
}

void ab_flush(abuf *ab) {
    if(!ab)
        return;
    memset(ab->b, 0, ab->size);
    ab->len=0;
}

void ab_rub_out(abuf *ab) {
    if(!ab || ab->len<1)
        return;
    ab->b[--ab->len]='\0';
}
