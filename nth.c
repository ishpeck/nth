/* Nth -- An editor forked from Kilo by Salvatore Sanfilippo.
 *
 * Copyright (C) 2017 Anthony "Ishpeck" Tedjamulia <nth AT ishpeck DOT net>
 *
 * -----------------------------------------------------------------------
 * Kilo -- A very simple editor in less than 1-kilo lines of code (as counted
 *         by "cloc"). Does not depend on libcurses, directly emits VT100
 *         escapes on the terminal.
 *
 * -----------------------------------------------------------------------
 *
 * Copyright (C) 2016 Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "nth.h"

static struct termios orig_termios;

int parse_number(const char *rawNumber) {
    int number = -1;
    char *badness=NULL;
    if(!rawNumber)
        return -1;
    number = (int)strtol(rawNumber, &badness, 10);
    if(badness==rawNumber)
        return -1;
    return number;
}

void editor_cleanup(void) {
    ab_destroy(edState.inputCommand);
    if (!edState.rawmode)
        return;
    write(STDOUT_FILENO, CTRLSEQ_GO_HOME, strlen(CTRLSEQ_GO_HOME));
    write(STDOUT_FILENO, CTRLSEQ_CLEAR_SCREEN, strlen(CTRLSEQ_CLEAR_SCREEN));
    tcsetattr(STDIN_FILENO,TCSAFLUSH,&orig_termios);
    edState.rawmode = 0;
}

coordinates doc_position_of_cursor(const editorConfig ed) {
    coordinates docPos;
    docPos.y = ed.scrPos.y+ed.cursor.y;
    docPos.x = ed.scrPos.x+ed.cursor.x;
    return docPos;
}

int is_cursor_at_line_ending(const editorConfig ed) {
    coordinates docPos = doc_position_of_cursor(ed);
    return docPos.x>=ed.row[docPos.y].size;
}

int is_region_set(const editorConfig ed) {
    return ed.markPos.x>=0 && ed.markPos.y>=0;
}

coordinates region_beginning(const editorConfig ed) {
    coordinates docPos = doc_position_of_cursor(ed);
    if(docPos.y < ed.markPos.y || (docPos.y == ed.markPos.y && docPos.x < ed.markPos.x))
        return docPos;
    return ed.markPos;
}

coordinates region_ending(const editorConfig ed) {
    coordinates docPos = doc_position_of_cursor(ed);
    if(docPos.y > ed.markPos.y || (docPos.y==ed.markPos.y && docPos.x > ed.markPos.x))
        return docPos;
    return ed.markPos;
}

int is_point_within_region(const editorConfig ed, int x, int y) {
    coordinates beginning;
    coordinates ending;
    if(!is_region_set(ed))
        return 0;
    beginning = region_beginning(ed);
    ending = region_ending(ed);
    if(y<beginning.y || y>ending.y)
        return 0;
    if(y==beginning.y && x<beginning.x)
        return 0;
    if(y==ending.y && x>ending.x)
        return 0;
    return 1;
}

void set_status_to_some_debug_info() {
    coordinates docPos = doc_position_of_cursor(edState);
    coordinates beginning = region_beginning(edState);
    coordinates ending = region_ending(edState);
    set_status("%d,%d -- %d,%d\tML: %d",
               beginning.x,
               beginning.y,
               ending.x,
               ending.y,
               edState.row[docPos.y].hl_openComment);
}

void horizontally_orient_screen(void) {
    int diff = edState.cursor.x - edState.scrSize.x;
    if (diff < 0)
        return;
    edState.cursor.x -= diff;
    edState.scrPos.x += diff;
}

int enable_raw_term_mode(int fd) {
    struct termios raw;

    if (edState.rawmode)
        return 0;
    if (!isatty(STDIN_FILENO))
        goto fatal;
    atexit(editor_cleanup);
    if (tcgetattr(fd,&orig_termios) == -1)
        goto fatal;

    raw = orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 1;

    if (tcsetattr(fd,TCSAFLUSH,&raw) < 0)
        goto fatal;
    edState.rawmode = 1;
    return 0;

fatal:
    errno = ENOTTY;
    return -1;
}

int character_at_cursor(editorConfig ed) {
    coordinates docPos = doc_position_of_cursor(ed);
    return ed.row[docPos.y].chars[docPos.x];
}

int read_in_meta_modified_keypresses(char input) {
    switch(input) {
        case KEY_NULL: return CTRL_META_SPACE;
        case ENTER: return META_ENTER;
        case TAB: return META_TAB;
        case CTRL_A: return META_CTRL_A;
        case CTRL_B: return META_CTRL_B;
        case CTRL_N: return META_CTRL_N;
        case CTRL_P: return META_CTRL_P;
        case 'f': return META_F;
        case 'g': return META_G;
        case 'b': return META_B;
        case 'r': return META_R;
        case 't': return META_T;
        case 'v': return PAGE_UP;
        case 'w': return META_W;
        case '<': return META_CROCK;
        case '>': return META_GATOR;
        case '!': return META_BANG;
        case '|': return META_PIPE;
        case '[': return META_OPEN_BRACKET;
        case '{': return META_OPEN_BRACE;
        case '(': return META_OPEN_PAREN;
        case '.': return META_DOT;
        case ',': return META_COMMA;
    }
    return ESC;
}

int read_escape_sequences_from_term(int fd) {
    char seq[3]={'\0', '\0', '\0'};

    if (read(fd,seq,1) == 0)
        return ESC;

    if (read(fd,seq+1,1) == 0)
        return read_in_meta_modified_keypresses(seq[0]);

    if (seq[0] == 'O') {
        switch(seq[1]) {
            case 'P': return FUNCTION_1;
            case 'Q': return FUNCTION_2;
            case 'R': return FUNCTION_3;
        }
    }

    if (seq[0] == '[') {
        if (seq[1] == 'P')
            return DEL_KEY;
        if (seq[1] >= '0' && seq[1] <= '9') {
            if (read(fd,seq+2,1) == 0)
                return ESC;
            if (seq[2] == '~') {
                switch(seq[1]) {
                    case '1': return HOME_KEY;
                    case '3': return DEL_KEY;
                    case '4': return END_KEY;
                    case '5': return PAGE_UP;
                    case '6': return PAGE_DOWN;
                }
            }
        } 
        switch(seq[1]) {
            case 'A': return ARROW_UP;
            case 'B': return ARROW_DOWN;
            case 'C': return ARROW_RIGHT;
            case 'D': return ARROW_LEFT;
            case 'F': return END_KEY;
            case 'H': return HOME_KEY;
            case 'Z': return SHIFT_TAB;
        }
    }
    return ESC;
}

int read_keypresses_from_raw_terminal(int fd) {
    int nread;
    char c;
    while ((nread = read(fd,&c,1)) == 0);
    if (nread == -1)
        exit(1);

    switch(c) {
        case ESC:
            return read_escape_sequences_from_term(fd);
        default:
            return c;
    }
}

int get_cursor_pos(int ifd, int ofd, int *out_rows, int *out_cols) {
    char buf[32];
    unsigned int i = 0;

    if (write(ofd, CTRLSEQ_GET_CURSOR_POS, 4) != 4)
        return -1;

    while (i < sizeof(buf)-1) {
        if (read(ifd,buf+i,1) != 1)
            break;
        if (buf[i] == 'R')
            break;
        i++;
    }
    buf[i] = '\0';

    if (buf[0] != ESC || buf[1] != '[')
        return -1;
    if (sscanf(buf+2,"%d;%d",out_rows,out_cols) != 2)
        return -1;
    return 0;
}

/* Try to get the number of columns in the current terminal. If the ioctl()
 * call fails the function will try to query the terminal itself.
 * Returns 0 on success, -1 on error. */
int get_window_size(int ifd, int ofd, int *out_rows, int *out_cols) {
    struct winsize ws;

    if (ioctl(1, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
        int orig_row, orig_col, retval;

        retval = get_cursor_pos(ifd,ofd,&orig_row,&orig_col);
        if (retval == -1)
            goto failed;

        if (write(ofd,"\x1b[999C\x1b[999B",12) != 12)
            goto failed;
        retval = get_cursor_pos(ifd,ofd,out_rows,out_cols);
        if (retval == -1)
            goto failed;

        /* Restore position. */
        char seq[32];
        snprintf(seq,32,"\x1b[%d;%dH",orig_row,orig_col);
        if (write(ofd,seq,strlen(seq)) == -1)
            set_status("ERROR: Couldn't restore position!");
        return 0;
    } else {
        *out_cols = ws.ws_col;
        *out_rows = ws.ws_row;
        return 0;
    }

failed:
    return -1;
}

int is_separator(int c) {
    return c == '\0' || isspace(c) || strchr(",.()+-/*=~%[];",c) != NULL;
}

int is_word_boundary(int c) {
    return c == '\0' || isspace(c) || strchr(",.()+-/*=~%[]<>{}:?!@#&|;\\",c) != NULL;
}

void jump_cursor_by_word(const int direction) {
    int redraw=0;
    while(isspace(character_at_cursor(edState)))
        redraw=push_cursor_with(direction) || redraw;
    do
        redraw=push_cursor_with(direction) || redraw;
    while(!is_word_boundary(character_at_cursor(edState)));
    if(redraw)
        redraw_region(region_beginning(edState),
                      region_ending(edState));
}

int jump_to_command_line_number(int isDone, coordinates originalCursor) {
    int desiredLine=parse_number(ab_content(edState.inputCommand));
    int increment=1;
    coordinates docPos = doc_position_of_cursor(edState);
    if(isDone)
        set_status("Moved here from %d,%d", originalCursor.x, originalCursor.y);
    if(desiredLine<0 || desiredLine>edState.numrows || isDone)
        return NTH_COMMAND_BAD;
    if(docPos.y>desiredLine)
        increment=-1;
    while(docPos.y+1!=desiredLine) {
        edState.cursor.y+=increment;
        docPos = doc_position_of_cursor(edState);
    }
    center_screen_upon_cursor();
    return NTH_COMMAND_OK;
}

int find_previous_line_with_matching_text(const char *searchTerm) {
    coordinates docPos=doc_position_of_cursor(edState);
    for(;docPos.y>=0;docPos=doc_position_of_cursor(edState)) {
        if(strstr(edState.row[docPos.y].render, searchTerm))
            return docPos.y;
        move_cursor(ARROW_UP);
    }
    return -1;
}

int find_next_line_with_matching_text(const char *searchTerm) {
    coordinates docPos=doc_position_of_cursor(edState);
    for(;docPos.y<edState.numrows;docPos=doc_position_of_cursor(edState)) {
        if(strstr(edState.row[docPos.y].render, searchTerm))
            return docPos.y;
        move_cursor(ARROW_DOWN);
    }
    return -1;
}

int search_for_next_command_text(int isDone, coordinates originalCursor) {
    int matchingLine=find_next_line_with_matching_text(ab_content(edState.inputCommand));
    if(isDone)
        return NTH_COMMAND_OK;
    if(matchingLine<0) {
        edState.scrPos.y=originalCursor.y;
        edState.cursor.y=0;
    }
    center_screen_upon_cursor();
    return (matchingLine<0) ? NTH_COMMAND_BAD : NTH_COMMAND_OK;
}

int search_for_previous_command_text(int isDone, coordinates originalCursor) {
    int matchingLine=find_previous_line_with_matching_text(ab_content(edState.inputCommand));
    if(isDone)
        return NTH_COMMAND_OK;
    if(matchingLine<0) {
        edState.scrPos.y=originalCursor.y;
        edState.cursor.y=0;
    }
    center_screen_upon_cursor();
    return (matchingLine<0) ? NTH_COMMAND_ABORT : NTH_COMMAND_OK;
}

void search_repeat_forward_search(void) {
    move_cursor(ARROW_DOWN);
}

void search_repeat_back_search(void) {
    move_cursor(ARROW_UP);
}


void read_command(int fd, const char *prompt, int specialKey, command_processor proc, command_special specialProc) {
    int c;
    int status = NTH_COMMAND_OK;
    coordinates docPos = doc_position_of_cursor(edState);
    while(status!=NTH_COMMAND_ABORT) {
        set_status("%s%s: %s",
                   (status==NTH_COMMAND_OK) ? "": "!!! ",
                   prompt,
                   ab_content(edState.inputCommand));
        refresh_screen();
        c = read_keypresses_from_raw_terminal(fd);
        switch(c) {
            case DEL_KEY: case CTRL_H: case BACKSPACE:
                ab_rub_out(edState.inputCommand);
                break;
            case CTRL_U:
                ab_flush(edState.inputCommand);
                break;
            case ESC: case CTRL_G: case ENTER: case CTRL_SLASH: case CTRL_Q:
                goto command_completed;
            case TAB:
                break;
            default:
                if(c==specialKey && specialProc!=NULL)
                    specialProc();
                if(isprint(c))
                    ab_attach(edState.inputCommand, c);
        }
        status=proc(NTH_COMMAND_ONGOING, docPos);
    }
command_completed:
    proc(NTH_COMMAND_FINISHED, docPos);
}

/* ====================== Syntax highlight color scheme  ==================== */

/* Return true if the specified row last char is part of a multi line comment
 * that starts at this row or at one before, and does not end at the end
 * of the row but spawns to the next row. */
int editor_row_has_open_comment(erow *row) {
    if (row->hl && row->rsize && row->hl[row->rsize-1] == HL_MLCOMMENT &&
        (row->rsize < 2 || (row->render[row->rsize-2] != '*' ||
                            row->render[row->rsize-1] != '/')))
        return 1;
    return 0;
}

int editor_row_has_open_region(erow *row) {
    coordinates beginning = region_beginning(edState);
    coordinates ending = region_ending(edState);
    return row->idx>=beginning.y && row->idx<ending.y;
}

/* Set every byte of row->hl (that corresponds to every character in the line)
 * to the right syntax highlight type (HL_* defines). */
void syntax_paint_row(erow *row) {
    row->hl = realloc(row->hl,row->rsize);
    memset(row->hl,HL_NORMAL,row->rsize);

    if (edState.syntax == NULL)
        return; /* No syntax, everything is HL_NORMAL. */

    int i, prev_sep, in_string, in_comment, in_region;
    char *p;
    char **keywords = edState.syntax->keywords;
    char *scs = edState.syntax->singleline_comment_start;
    char *mcs = edState.syntax->multiline_comment_start;
    char *mce = edState.syntax->multiline_comment_end;

    /* Point to the first non-space char. */
    p = row->render;
    i = 0; /* Current char offset */
    while(*p && isspace(*p)) {
        row->hl[i]=HL_NORMAL;
        p++;
        i++;
    }
    prev_sep = 1; /* Tell the parser if 'i' points to start of word. */
    in_string = 0; /* Are we inside "" or '' ? */
    in_comment = 0; /* Are we inside multi-line comment? */
    in_region = 0;

    /* If the previous line has an open comment, this line starts
     * with an open comment state. */
    if (row->idx > 0 && editor_row_has_open_comment(&edState.row[row->idx-1]))
        in_comment = 1;

    while(*p) {
        /* Handle // comments. */
        if (prev_sep && *p == scs[0] && *(p+1) == scs[1]) {
            /* From here to end is a comment */
            memset(row->hl+i,HL_COMMENT,row->size-i);
            return;
        }

        if (in_string) {
            row->hl[i] = HL_STRING;
            if (*p == '\\') {
                row->hl[i+1] = HL_STRING;
                p += 2; i += 2;
                prev_sep = 0;
                continue;
            }
            if (*p == in_string)
                in_string = 0;
            p++; i++;
            continue;
        } else {
            if (!in_comment && (*p == '"' || *p == '\'')) {
                in_string = *p;
                row->hl[i] = HL_STRING;
                p++; i++;
                prev_sep = 0;
                continue;
            }
        }

        /* Handle multi line comments. */
        if (in_comment) {
            row->hl[i] = HL_MLCOMMENT;
            if (*p == mce[0] && *(p+1) == mce[1]) {
                row->hl[i+1] = HL_MLCOMMENT;
                p += 2; i += 2;
                in_comment = 0;
                prev_sep = 1;
                continue;
            } else {
                prev_sep = 0;
                p++; i++;
                continue;
            }
        } else if (*p == mcs[0] && *(p+1) == mcs[1]) {
            row->hl[i] = HL_MLCOMMENT;
            row->hl[i+1] = HL_MLCOMMENT;
            p += 2; i += 2;
            in_comment = 1;
            prev_sep = 0;
            continue;
        }

        /* Handle non printable chars. */
        if (!isprint(*p)) {
            row->hl[i] = HL_NONPRINT;
            p++; i++;
            prev_sep = 0;
            continue;
        }

        /* Handle numbers */
        if ((isdigit(*p) && (prev_sep || row->hl[i-1] == HL_NUMBER)) ||
            (*p == '.' && i >0 && row->hl[i-1] == HL_NUMBER)) {
            row->hl[i] = HL_NUMBER;
            p++; i++;
            prev_sep = 0;
            continue;
        }

        /* Handle keywords and lib calls */
        if (prev_sep) {
            int j;
            for (j = 0; keywords[j]; j++) {
                int klen = strlen(keywords[j]);
                int kw2 = keywords[j][klen-1] == '|';
                if (kw2) klen--;

                if (!memcmp(p,keywords[j],klen) &&
                    is_separator(*(p+klen)))
                {
                    /* Keyword */
                    memset(row->hl+i,kw2 ? HL_KEYWORD2 : HL_KEYWORD1,klen);
                    p += klen;
                    i += klen;
                    break;
                }
            }
            if (keywords[j] != NULL) {
                prev_sep = 0;
                continue; /* We had a keyword match */
            }
        }

        /* Not special chars */
        prev_sep = is_separator(*p);
        p++; i++;
    }

    /* Propagate syntax change to the next row if the open commen
     * state changed. This may recursively affect all the following rows
     * in the file. */
    int oc = editor_row_has_open_comment(row);
    if (row->hl_openComment != oc && row->idx+1 < edState.numrows)
        syntax_paint_row(&edState.row[row->idx+1]);
    row->hl_openComment = oc;
}

/* Select the syntax highlight scheme depending on the filename,
 * setting it in the global state edState.syntax. */
void syntax_color_scheme(char *filename) {
    for (unsigned int j = 0; j < HLDB_ENTRIES; j++) {
        editorSyntax *s = HLDB+j;
        unsigned int i = 0;
        while(s->filematch[i]) {
            char *p;
            int patlen = strlen(s->filematch[i]);
            if ((p = strstr(filename,s->filematch[i])) != NULL) {
                if (s->filematch[i][0] != '.' || p[patlen] == '\0') {
                    edState.syntax = s;
                    return;
                }
            }
            i++;
        }
    }
}

/* ======================= Editor rows implementation ======================= */

/* Update the rendered version and the syntax highlight of a row. */
void prepare_row_for_screen_rendering(erow *row) {
    int tabs = 0, nonprint = 0, j, idx;

   /* Create a version of the row we can directly print on the screen,
    * respecting tabs. */
    free(row->render);
    for (j = 0; j < row->size; j++)
        if (row->chars[j] == TAB) tabs++;

    row->render = malloc(row->size + tabs*edState.tabWidth + nonprint*9 + 1);
    idx = 0;
    for (j = 0; j < row->size; j++) {
        if (row->chars[j] == TAB) {
            row->render[idx++] = ' ';
            while((idx+1) % edState.tabWidth != 0)
                row->render[idx++] = ' ';
        } else {
            row->render[idx++] = row->chars[j];
        }
    }
    row->rsize = idx;
    row->render[idx] = '\0';

    /* Update the syntax highlighting attributes of the row. */
    syntax_paint_row(row);
}

void set_mark_to_cursor(void) {
    edState.markPos = doc_position_of_cursor(edState);
}

void redraw_region(coordinates beginning, coordinates ending) {
    int currentRowIdx;
    for(currentRowIdx=beginning.y;
        currentRowIdx<=ending.y && currentRowIdx<edState.numrows;
        ++currentRowIdx)
        syntax_paint_row(&edState.row[currentRowIdx]);
}

void clear_mark(void) {
    coordinates beginning = region_beginning(edState);
    coordinates ending = region_ending(edState);
    if(edState.markPos.x<0 || edState.markPos.y<0)
        return;
    edState.markPos.x = -1;
    edState.markPos.y = -1;
    redraw_region(beginning, ending);
}

int size_of_region(editorConfig ed) {
    coordinates beginning = region_beginning(ed);
    coordinates ending = region_ending(ed);
    int totalSize=ending.x, idx;
    totalSize-=beginning.x;
    for (idx=beginning.y;idx<ending.y;++idx)
        totalSize+=ed.row[idx].size+1;
    return totalSize;
}

char *stuff_in_region(editorConfig ed) {
    int regionSize=size_of_region(ed);
    char *stuff = calloc(regionSize+1, sizeof(char));
    char *point;
    coordinates beginning = region_beginning(ed);
    coordinates ending = region_ending(ed);
    int idx=beginning.y;
    point = ed.row[idx].chars+beginning.x;
    memcpy(stuff, point, ed.row[idx].size-beginning.x);
    point=stuff+beginning.x;
    if(beginning.y!=ending.y) {
        for(++idx;idx<ending.y;++idx) {
            memcpy(point, ed.row[idx].chars, ed.row[idx].size);
            point+=ed.row[idx].size;
        }
        ++idx;
        memcpy(point, ed.row[idx].chars, ending.x);
    }
    return stuff;
}

int matching_brace_character(const int subject) {
    switch(subject) {
        case '[': return ']';
        case ']': return '[';
        case '{': return '}';
        case '}': return '{';
        case ')': return '(';
        default: return ')';
    }
}

int is_brace(const int subject) {
    switch(subject) {
        case '[':
        case ']':
        case '{':
        case '}':
        case ')':
        case '(':
            return 1;
        default: return 0;
    }
}

void move_to_any_brace(const int direction) {
    int redraw = 0;
    do
        redraw = push_cursor_with(direction) || redraw;
    while(!is_brace(character_at_cursor(edState)));
    if(!redraw)
        return;
    redraw_region(region_beginning(edState), region_ending(edState));
}

void find_matching_brace(const int starting,
                         const int ending,
                         const int direction) {
    int neededMatches=1;
    int subject;
    coordinates docPos;
    do {
        move_cursor(direction);
        subject = character_at_cursor(edState);
        docPos = doc_position_of_cursor(edState);
        if(subject==starting)
            ++neededMatches;
        if(docPos.x==0 && docPos.y==0)
            return;
        if(subject==ending && --neededMatches<1)
            return;
    } while(1);
}

void find_accompanying_brace(const int direction) {
    int starting = character_at_cursor(edState);
    if(starting==0) {
        move_cursor(direction);
        starting = character_at_cursor(edState);
    }
    if(!is_brace(starting))
        move_to_any_brace(direction);
    else
        find_matching_brace(starting,
                            matching_brace_character(starting),
                            direction);
}

void send_region_to_program() {
    char *content=NULL;
    if (!is_region_set(edState)) {
        set_status("Marked region not set.");
        return;
    }
    content=stuff_in_region(edState);
    set_status("%s", content);
    free(content);
}

void call_subshell() {
    set_status("Region is %d bytes.", size_of_region(edState));
}

void editor_insert_row(int at, char *s, size_t len) {
    if (at > edState.numrows)
        return;
    edState.row = realloc(edState.row,sizeof(erow)*(edState.numrows+1));
    if (at != edState.numrows) {
        memmove(edState.row+at+1,edState.row+at,sizeof(edState.row[0])*(edState.numrows-at));
        for (int j = at+1; j <= edState.numrows; j++)
            edState.row[j].idx++;
    }
    edState.row[at].size = len;
    edState.row[at].chars = malloc(len+1);
    memcpy(edState.row[at].chars,s,len+1);
    edState.row[at].hl = NULL;
    edState.row[at].render = NULL;
    edState.row[at].rsize = 0;
    edState.row[at].hl_openComment = 0;
    edState.row[at].idx = at;
    prepare_row_for_screen_rendering(edState.row+at);
    edState.numrows++;
    edState.dirty++;
}

void editor_free_row(erow *row) {
    free(row->render);
    free(row->chars);
    free(row->hl);
}

void editor_delete_row(int at) {
    erow *row;

    if (at >= edState.numrows)
        return;
    row = edState.row+at;
    editor_free_row(row);
    memmove(edState.row+at,edState.row+at+1,sizeof(edState.row[0])*(edState.numrows-at-1));
    for (int j = at; j < edState.numrows-1; j++)
        edState.row[j].idx++;
    edState.numrows--;
    edState.dirty++;
}

/* Turn the editor rows into a single heap-allocated string.
 * Returns the pointer to the heap-allocated string and populate the
 * integer pointed by 'buflen' with the size of the string, escluding
 * the final nulterm. */
char *copy_rows_to_contiguous_string(int *buflen) {
    char *buf = NULL, *p;
    int totlen = 0;
    int j;

    /* Compute count of bytes */
    for (j = 0; j < edState.numrows; j++)
        totlen += edState.row[j].size+1; /* +1 is for "\n" at end of every row */
    *buflen = totlen;
    totlen++; /* Also make space for nulterm */

    p = buf = malloc(totlen);
    for (j = 0; j < edState.numrows; j++) {
        memcpy(p,edState.row[j].chars,edState.row[j].size);
        p += edState.row[j].size;
        *p = '\n';
        p++;
    }
    *p = '\0';
    return buf;
}

/* Insert a character at the specified position in a row, moving the remaining
 * chars on the right if needed. */
void insert_char_to_row(erow *row, int at, int c) {
    if (at > row->size) {
        /* Pad the string with spaces if the insert location is outside the
         * current length by more than a single character. */
        int padlen = at-row->size;
        /* In the next line +2 means: new char and null term. */
        row->chars = realloc(row->chars,row->size+padlen+2);
        memset(row->chars+row->size,' ',padlen);
        row->chars[row->size+padlen+1] = '\0';
        row->size += padlen+1;
    } else {
        /* If we are in the middle of the string just make space for 1 new
         * char plus the (already existing) null term. */
        row->chars = realloc(row->chars,row->size+2);
        memmove(row->chars+at+1,row->chars+at,row->size-at+1);
        row->size++;
    }
    row->chars[at] = c;
    prepare_row_for_screen_rendering(row);
    edState.dirty++;
}

/* Append the string 's' at the end of a row */
void append_str_to_row(erow *row, char *s, size_t len) {
    row->chars = realloc(row->chars,row->size+len+1);
    memcpy(row->chars+row->size,s,len);
    row->size += len;
    row->chars[row->size] = '\0';
    prepare_row_for_screen_rendering(row);
    edState.dirty++;
}

/* Delete the character at offset 'at' from the specified row. */
void delete_char_from_row(erow *row, int at) {
    if (row->size <= at)
        return;
    memmove(row->chars+at,row->chars+at+1,row->size-at);
    prepare_row_for_screen_rendering(row);
    row->size--;
    edState.dirty++;
}

/* Insert the specified char at the current prompt position. */
void editor_insert_character(int c) {
    coordinates docPos = doc_position_of_cursor(edState);
    erow *row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];

    /* If the row where the cursor is currently located does not exist in our
     * logical representaion of the file, add enough empty rows as needed. */
    if (!row) {
        while(edState.numrows <= docPos.y)
            editor_insert_row(edState.numrows,"",0);
    }
    row = &edState.row[docPos.y];
    insert_char_to_row(row,docPos.x,c);
    if (edState.cursor.x == edState.scrSize.x-1)
        edState.scrPos.x++;
    else
        edState.cursor.x++;
    edState.dirty++;
}

/* Inserting a newline is slightly complex as we have to handle inserting a
 * newline in the middle of a line, splitting the line as needed. */
void insert_new_line(void) {
    coordinates docPos = doc_position_of_cursor(edState);
    erow *row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];

    if (!row) {
        if (docPos.y == edState.numrows) {
            editor_insert_row(docPos.y,"",0);
            goto fixcursor;
        }
        return;
    }
    /* If the cursor is over the current line size, we want to conceptually
     * think it's just over the last character. */
    if (docPos.x >= row->size)
        docPos.x = row->size;
    if (docPos.x == 0) {
        editor_insert_row(docPos.y,"",0);
    } else {
        /* We are in the middle of a line. Split it between two rows. */
        editor_insert_row(docPos.y+1,row->chars+docPos.x,row->size-docPos.x);
        row = &edState.row[docPos.y];
        row->chars[docPos.x] = '\0';
        row->size = docPos.x;
        prepare_row_for_screen_rendering(row);
    }
fixcursor:
    if (edState.cursor.y == edState.scrSize.y-1) {
        edState.scrPos.y++;
    } else {
        edState.cursor.y++;
    }
    edState.cursor.x = 0;
    edState.scrPos.x = 0;
}

void insert_new_line_after_cursor(void) {
    coordinates origCursor = edState.cursor;
    insert_new_line();
    edState.cursor=origCursor;
}

void editor_rub_out(int rightwardHops) {
    coordinates docPos;
    edState.cursor.x+=rightwardHops;
    docPos = doc_position_of_cursor(edState);
    erow *row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];

    if (!row || (docPos.x == 0 && docPos.y == 0))
        return;

    if (rightwardHops>0 && docPos.x>row->size) {
        edState.cursor.x=0;
        edState.cursor.y+=1;
        docPos = doc_position_of_cursor(edState);
        row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];
    }

    if (docPos.x == 0) {
        /* Handle the case of column 0, we need to move the current line
         * on the right of the previous one. */
        docPos.x = edState.row[docPos.y-1].size;
        append_str_to_row(&edState.row[docPos.y-1],row->chars,row->size);
        editor_delete_row(docPos.y);
        row = NULL;
        if (edState.cursor.y == 0)
            edState.scrPos.y--;
        else
            edState.cursor.y--;
        edState.cursor.x = docPos.x;
        if (edState.cursor.x >= edState.scrSize.x) {
            int shift = (edState.scrSize.x-edState.cursor.x)+1;
            edState.cursor.x -= shift;
            edState.scrPos.x += shift;
        }
    } else {
        delete_char_from_row(row,docPos.x-1);
        if (edState.cursor.x == 0 && edState.scrPos.x)
            edState.scrPos.x--;
        else
            edState.cursor.x--;
    }
    if (row) prepare_row_for_screen_rendering(row);
    edState.dirty++;
}

void transpose_characters(void) {
    coordinates docPos = doc_position_of_cursor(edState);
    int right;
    int left;
    if(docPos.x>=edState.row[docPos.y].size)
        move_cursor(ARROW_LEFT);
    if(docPos.x==0)
        move_cursor(ARROW_RIGHT);
    right=character_at_cursor(edState);
    move_cursor(ARROW_LEFT);
    left=character_at_cursor(edState);
    editor_rub_out(1);
    editor_rub_out(1);
    editor_insert_character(right);
    editor_insert_character(left);
}

void obliterate_word(void) {
    while(isspace(character_at_cursor(edState))) {
        editor_rub_out(1);
        move_cursor(ARROW_LEFT);
    }
    if(is_word_boundary(character_at_cursor(edState))) {
        editor_rub_out(1);
        return;
    }
    while(!is_word_boundary(character_at_cursor(edState))) {
        editor_rub_out(1);
        move_cursor(ARROW_LEFT);
    }
}

void obliterate_region(void) {
    int deletions, delDir=0;
    coordinates beginning, ending, docPos;
    if(!is_region_set(edState))
        return;
    beginning=region_beginning(edState);
    ending=region_ending(edState);
    docPos=doc_position_of_cursor(edState);
    if(docPos.x==beginning.x && docPos.y==beginning.y)
        delDir=1;
    for(deletions=size_of_region(edState);deletions>0;--deletions)
        editor_rub_out(delDir);
}

void delete_region_or_character(int delDir) {
    if(is_region_set(edState))
        obliterate_region();
    else
        editor_rub_out(delDir);
}

int open_file(char *filename) {
    FILE *fp;

    edState.dirty = 0;
    free(edState.filename);
    edState.filename = strdup(filename);

    fp = fopen(filename,"r");
    if (!fp) {
        if (errno != ENOENT) {
            perror("Opening file");
            exit(1);
        }
        return 1;
    }

    char *line = NULL;
    size_t linecap = 0;
    ssize_t linelen;
    while((linelen = getline(&line,&linecap,fp)) != -1) {
        if (linelen && (line[linelen-1] == '\n' || line[linelen-1] == '\r'))
            line[--linelen] = '\0';
        editor_insert_row(edState.numrows,line,linelen);
    }
    free(line);
    fclose(fp);
    edState.dirty = 0;
    return 0;
}

/* Save the current file on disk. Return 0 on success, 1 on error. */
int save_file(void) {
    int len;
    char *buf = copy_rows_to_contiguous_string(&len);
    int fd = open(edState.filename,O_RDWR|O_CREAT,0644);
    if (fd == -1)
        goto writeerr;

    /* Use truncate + a single write(2) call in order to make saving
     * a bit safer, under the limits of what we can do in a small editor. */
    if (ftruncate(fd,len) == -1)
        goto writeerr;
    if (write(fd,buf,len) != len)
        goto writeerr;

    close(fd);
    free(buf);
    edState.dirty = 0;
    set_status("%d bytes written on disk", len);
    return 0;

writeerr:
    free(buf);
    if (fd != -1) close(fd);
    set_status("Can't save! I/O error: %s",strerror(errno));
    return 1;
}

int syntax_ensure_region_coloring(abuf *ab,
                                  int currentColor,
                                  int newColor,
                                  int prevRegion,
                                  int x,
                                  int y) {
    int isRegionized = is_point_within_region(edState, x, y);
    if(isRegionized==prevRegion && currentColor==newColor)
        return isRegionized;
    if(prevRegion && !isRegionized)
        ab_append(ab, CTRLSEQ_COLOR_RESET, 4);
    switch(newColor) {
        case HL_COMMENT:
        case HL_MLCOMMENT:
            ab_append(ab, CTRLSEQ_FG_GREEN, 5);
            break;
        case HL_KEYWORD1:
            ab_append(ab, CTRLSEQ_TEXT_BOLD, 4);
            ab_append(ab, CTRLSEQ_FG_YELLOW, 5);
            break;
        case HL_KEYWORD2:
            ab_append(ab, CTRLSEQ_TEXT_BOLD, 4);
            ab_append(ab, CTRLSEQ_FG_BLUE, 5);
            break;
        case HL_STRING:
            ab_append(ab, CTRLSEQ_FG_CYAN, 5);
            break;
        case HL_NUMBER:
            ab_append(ab, CTRLSEQ_FG_MAGENTA, 5);
            break;
        case HL_MATCH:
            ab_append(ab, CTRLSEQ_FG_BLUE, 5);
            break;
        case HL_NORMAL:
        default:
            ab_append(ab, CTRLSEQ_COLOR_RESET, 4);
            if(isRegionized) {
                ab_append(ab, CTRLSEQ_COLOR_INVERT, 4);
                prevRegion=isRegionized;
            }
            break;
    }
    if(!prevRegion && isRegionized)
        ab_append(ab, CTRLSEQ_COLOR_INVERT, 4);
    return isRegionized;
}

void refresh_screen_draw_to_buffer(abuf *ab) {
    int y, filerow, isRegionized=-1;
    erow *r;
    char buf[32];

    ab_append(ab,CTRLSEQ_HIDE_CURSOR,6);
    ab_append(ab,CTRLSEQ_GO_HOME,3);
    if(is_point_within_region(edState, 0, edState.scrPos.y))
        ab_append(ab,CTRLSEQ_COLOR_INVERT,4);
    for (y = 0; y < edState.scrSize.y; y++) {
        filerow = edState.scrPos.y+y;
        if (filerow >= edState.numrows) {
            if (edState.numrows == 0 && y == edState.scrSize.y/3) {
                char welcome[80];
                int welcLen = snprintf(welcome,
                                       sizeof(welcome),
                                       "Nth editor -- verison %s"
                                       CTRLSEQ_CLEAR_TO_EOL
                                       "\r\n",
                                       NTH_VERSION);
                int padding = (edState.scrSize.x-welcLen)/2;
                if (padding) {
                    ab_append(ab,"~",1);
                    padding--;
                }
                while(padding--) ab_append(ab," ",1);
                ab_append(ab,welcome,welcLen);
            } else {
                ab_append(ab, "~" CTRLSEQ_CLEAR_TO_EOL "\r\n", 7);
            }
            continue;
        }

        r = &edState.row[filerow];
        int len = r->rsize - edState.scrPos.x;
        int current_color = -1;

        if (len > 0) {
            if (len > edState.scrSize.x)
                len = edState.scrSize.x;
            char *c = r->render+edState.scrPos.x;
            unsigned char *hl = r->hl+edState.scrPos.x;
            int j;
            for (j = 0; j < len; j++) {
                if (hl[j] == HL_NONPRINT) {
                    char sym;
                    ab_append(ab,CTRLSEQ_COLOR_INVERT,4);
                    sym = (c[j]<=26) ? '@'+c[j] : '?';
                    ab_append(ab,&sym,1);
                    ab_append(ab,CTRLSEQ_COLOR_RESET,4);
                } else {
                    int color = hl[j];
                    isRegionized=syntax_ensure_region_coloring(ab,
                                                               current_color,
                                                               color,
                                                               isRegionized,
                                                               j,
                                                               filerow);
                    current_color=color;
                    ab_append(ab,c+j,1);
                }
            }
        } else if(isRegionized) {
            ab_append(ab, " ", 1);
        }
        ab_append(ab,CTRLSEQ_FG_DEFAULT,5);
        ab_append(ab,CTRLSEQ_CLEAR_TO_EOL,4);
        ab_append(ab,"\r\n",2);
    }

    /* Create a two rows status. First row: */
    ab_append(ab,CTRLSEQ_CLEAR_TO_EOL,4);
    ab_append(ab,CTRLSEQ_COLOR_INVERT,4);
    char status[80], rstatus[80];
    int len = snprintf(status, sizeof(status), "%.20s - %d lines %s",
                       edState.filename, edState.numrows,
                       edState.dirty ? "(modified)" : "");
    int rlen = snprintf(rstatus, sizeof(rstatus),
        "%d/%d",edState.scrPos.y+edState.cursor.y+1,edState.numrows);
    if (len > edState.scrSize.x) len = edState.scrSize.x;
    ab_append(ab,status,len);
    while(len < edState.scrSize.x) {
        if (edState.scrSize.x - len == rlen) {
            ab_append(ab,rstatus,rlen);
            break;
        } else {
            ab_append(ab," ",1);
            len++;
        }
    }
    ab_append(ab,CTRLSEQ_COLOR_RESET"\r\n",6);

    /* Second row depends on edState.statusmsg and the status message update time. */
    ab_append(ab,CTRLSEQ_CLEAR_TO_EOL,4);
    int msglen = strlen(edState.statusmsg);
    if (msglen && time(NULL)-edState.statusmsg_time < 5)
        ab_append(ab,edState.statusmsg,msglen <= edState.scrSize.x ? msglen : edState.scrSize.x);

    /* Put cursor at its current position. Note that the horizontal position
     * at which the cursor is displayed may be different compared to 'edState.cursor.x'
     * because of TABs. */
    int j;
    int cx = 1;
    filerow = edState.scrPos.y+edState.cursor.y;
    erow *row = (filerow >= edState.numrows) ? NULL : &edState.row[filerow];
    if (row) {
        for (j = edState.scrPos.x; j < (edState.cursor.x+edState.scrPos.x); j++) {
            if (j < row->size && row->chars[j] == TAB) cx += 7-((cx)%8);
            cx++;
        }
    }
    snprintf(buf,sizeof(buf),"\x1b[%d;%dH",edState.cursor.y+1,cx);
    ab_append(ab,buf,strlen(buf));
    ab_append(ab,CTRLSEQ_SHOW_CURSOR,6);
    write(STDOUT_FILENO,ab_content(ab),ab_length(ab));
}

void refresh_screen(void) {
    abuf *ab = ab_init(32);
    refresh_screen_draw_to_buffer(ab);
    ab_destroy(ab);
}

/* Set an editor status message for the second line of the status, at the
 * end of the screen. */
void set_status(const char *fmt, ...) {
    va_list ap;
    va_start(ap,fmt);
    vsnprintf(edState.statusmsg,sizeof(edState.statusmsg),fmt,ap);
    va_end(ap);
    edState.statusmsg_time = time(NULL);
}

void move_cursor_by_page(int upOrDown) {
    int times = edState.scrSize.y;
    if(upOrDown==ARROW_UP && edState.cursor.y != 0)
        edState.cursor.y=0;
    else if (upOrDown==PAGE_DOWN && edState.cursor.y != edState.scrSize.y-1)
        edState.cursor.y = edState.scrSize.y-1;
    while(times--)
        move_cursor(upOrDown);
}

int push_cursor_with(const int key) {
    int rowlen, shallRedraw = 0;
    coordinates docPos = doc_position_of_cursor(edState);
    erow *row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];

    switch(key) {
        case META_CROCK:
            edState.cursor.y = 0;
            edState.scrPos.y = 0;
        case CTRL_A:
        case HOME_KEY:
            edState.cursor.x=0;
            edState.scrPos.x=0;
            shallRedraw = is_region_set(edState);
            break;
        case META_GATOR:
            edState.cursor.y = edState.scrSize.y-1;
            edState.scrPos.y = edState.numrows - edState.scrSize.y;
        case CTRL_E:
        case END_KEY:
            edState.cursor.x = edState.row[docPos.y].size;
            if (edState.cursor.x > edState.scrSize.x-1) {
                edState.scrPos.x = edState.cursor.x - edState.scrSize.x+1;
                edState.cursor.x = edState.scrSize.x-1;
            }
            shallRedraw = is_region_set(edState);
            break;
        case CTRL_B:
        case ARROW_LEFT:
            if (edState.cursor.x == 0) {
                if (edState.scrPos.x) {
                    edState.scrPos.x--;
                } else {
                    if (docPos.y > 0) {
                        edState.cursor.y--;
                        edState.cursor.x = edState.row[docPos.y-1].size;
                        if (edState.cursor.x > edState.scrSize.x-1) {
                            edState.scrPos.x = edState.cursor.x-edState.scrSize.x+1;
                            edState.cursor.x = edState.scrSize.x-1;
                        }
                    }
                }
            } else {
                edState.cursor.x -= 1;
            }
            shallRedraw = is_region_set(edState);
            break;
        case CTRL_F:
        case ARROW_RIGHT:
            if (row && docPos.x < row->size) {
                edState.cursor.x++;
                horizontally_orient_screen();
            } else if (row && docPos.x == row->size) {
                edState.cursor.x = 0;
                edState.scrPos.x = 0;
                if (edState.cursor.y == edState.scrSize.y-1) {
                    edState.scrPos.y++;
                } else {
                    edState.cursor.y += 1;
                }
            }
            shallRedraw = is_region_set(edState);
            break;
        case CTRL_P:
        case ARROW_UP:
            if (edState.cursor.y == 0) {
                if (edState.scrPos.y)
                    edState.scrPos.y--;
            } else {
                edState.cursor.y -= 1;
            }
            shallRedraw = is_region_set(edState);
            break;
        case CTRL_N:
        case ARROW_DOWN:
            if (docPos.y < edState.numrows) {
                if (edState.cursor.y == edState.scrSize.y-1) {
                    edState.scrPos.y++;
                } else {
                    edState.cursor.y += 1;
                }
            }
            shallRedraw = is_region_set(edState);
            break;
    }
    /* Fix cursor's X position if the current line is too short. */
    docPos.y = edState.scrPos.y+edState.cursor.y;
    docPos.x = edState.scrPos.x+edState.cursor.x;
    row = (docPos.y >= edState.numrows) ? NULL : &edState.row[docPos.y];
    rowlen = row ? row->size : 0;
    if (docPos.x > rowlen) {
        edState.cursor.x -= docPos.x-rowlen;
        if (edState.cursor.x < 0) {
            edState.scrPos.x += edState.cursor.x;
            edState.cursor.x = 0;
        }
    }
    return shallRedraw;
}

void move_cursor(const int key) {
    if(!push_cursor_with(key))
        return;
    redraw_region(region_beginning(edState),
                  region_ending(edState));
}

void center_screen_upon_cursor(void) {
    while(edState.cursor.y<edState.scrSize.y/2 && edState.scrPos.y>0) {
        --edState.scrPos.y;
        move_cursor(ARROW_DOWN);
    }
    while(edState.cursor.y>edState.scrSize.y/2 && edState.scrPos.y<edState.numrows) {
        ++edState.scrPos.y;
        move_cursor(ARROW_UP);
    }
}

void obliterate_to_start_of_row(void) {
    int repeats = edState.cursor.x;
    while(repeats-->0)
        editor_rub_out(0);
}

void obliterate_to_end_of_row(void) {
    coordinates docPos = doc_position_of_cursor(edState);
    int repeats = edState.row[docPos.y].size - edState.cursor.x;
    if(repeats<1) {
        move_cursor(ARROW_DOWN);
        move_cursor(HOME_KEY);
        editor_rub_out(0);
        return;
    }
    while(repeats-->0)
        editor_rub_out(1);
}

void indent_current_line(int tabWidth) {
    int spaces;
    coordinates originalPosition = edState.cursor;
    edState.cursor.x=0;
    for(spaces=tabWidth;spaces>0;--spaces)
        editor_insert_character(' ');
    edState.cursor=originalPosition;
    edState.cursor.x+=tabWidth;
}

void unindent_current_line(int tabWidth) {
    int spaces;
    coordinates originalPosition = edState.cursor;
    edState.cursor.x=0;
    for(spaces=tabWidth;spaces>0 && isspace(character_at_cursor(edState));--spaces)
        editor_rub_out(1);
    edState.cursor=originalPosition;
    edState.cursor.x-=tabWidth;
}

void indent_region(int tabWidth) {
    coordinates originalPosition = edState.cursor;
    coordinates beginning = region_beginning(edState);
    coordinates ending = region_ending(edState);
    edState.cursor=beginning;
    while(edState.cursor.y<=ending.y) {
        indent_current_line(tabWidth);
        edState.cursor.y++;
    }
    edState.cursor=originalPosition;
    edState.cursor.x+=tabWidth;
}

void unindent_region(int tabWidth) {
    coordinates originalPosition = edState.cursor;
    coordinates beginning = region_beginning(edState);
    coordinates ending = region_ending(edState);
    edState.cursor=beginning;
    while(edState.cursor.y<=ending.y) {
        unindent_current_line(tabWidth);
        edState.cursor.y++;
    }
    edState.cursor=originalPosition;
    edState.cursor.x-=tabWidth;
}

void indent_region_or_current_line(int tabWidth) {
    if(is_region_set(edState))
        indent_region(tabWidth);
    else
        indent_current_line(tabWidth);
}

void unindent_region_or_current_line(int tabWidth) {
    if(is_region_set(edState))
        unindent_region(tabWidth);
    else
        unindent_current_line(tabWidth);
}

void scroll_screen(int direction) {
    if(direction==0)
        return;
    edState.scrPos.y+=direction;
    if(edState.scrPos.y<1)
        edState.scrPos.y=0;
}

void ui_process_keypress(int fd) {
    static int quit_times = NTH_QUIT_TIMES;

    int c = read_keypresses_from_raw_terminal(fd);
    switch(c) {
        case TAB:
            indent_region_or_current_line(edState.tabWidth);
            break;
        case KEY_NULL:
            set_mark_to_cursor();
            set_status("Mark set to %d, %d", edState.markPos.x, edState.markPos.y);
            break;
        case ENTER:
            insert_new_line();
            break;
        case CTRL_T:
            transpose_characters();
            break;
        case META_T:
            set_status("Word transposition not implemented");
            break;
        case META_B:
            jump_cursor_by_word(ARROW_LEFT);
            break;
        case META_F:
            jump_cursor_by_word(ARROW_RIGHT);
            break;
        case META_G:
            read_command(STDIN_FILENO, "Goto line", 0, jump_to_command_line_number, NULL);
            break;
        case META_BANG:
            call_subshell();
            break;
        case META_W:
            set_status("Copy Region not implemented yet");
            break;
        case CTRL_W:
            if(is_cursor_at_line_ending(edState))
                move_cursor(ARROW_LEFT);
            obliterate_word();
            break;
        case CTRL_G:
            clear_mark();
            set_status("%c", character_at_cursor(edState));
            break;
        case META_PIPE:
            send_region_to_program();
            break;
        case CTRL_C:
            break;
        case CTRL_Q:
            if (edState.dirty && quit_times) {
                set_status("WARNING! File has unsaved changes. "
                           "Press Ctrl-Q %d more times to quit.", quit_times);
                quit_times--;
                return;
            }
            exit(0);
            break;
        case FUNCTION_1:
            set_status("Help not implemented yet");
            break;
        case FUNCTION_2:
            save_file();
            break;
        case FUNCTION_3:
            set_status_to_some_debug_info();
            break;
        case CTRL_S:
            read_command(STDIN_FILENO, "Search forward", c, search_for_next_command_text, search_repeat_forward_search);
            break;
        case CTRL_R:
            read_command(STDIN_FILENO, "Reverse search", c, search_for_previous_command_text, search_repeat_back_search);
            break;
        case CTRL_D:
            delete_region_or_character(1);
            break;
        case META_ENTER:
            insert_new_line();
            insert_new_line_after_cursor();
            break;
        case DEL_KEY:
            delete_region_or_character(1);
            break;
        case SHIFT_TAB:
            unindent_region_or_current_line(edState.tabWidth);
            break;
        case META_TAB:
            set_status("Match indentation level not implemented yet");
            break;
        case CTRL_O:
            insert_new_line_after_cursor();
            break;
        case BACKSPACE:
        case CTRL_H:
            delete_region_or_character(0);
            break;
        case PAGE_UP:
            move_cursor_by_page(ARROW_UP);
            break;
        case CTRL_V:
        case PAGE_DOWN:
            move_cursor_by_page(ARROW_DOWN);
            break;
        case CTRL_P:
        case CTRL_N:
        case CTRL_B:
        case CTRL_F:
        case CTRL_A:
        case CTRL_E:
        case META_CROCK:
        case META_GATOR:
        case HOME_KEY:
        case END_KEY:
        case ARROW_UP:
        case ARROW_DOWN:
        case ARROW_LEFT:
        case ARROW_RIGHT:
            move_cursor(c);
            break;
        case CTRL_K:
            obliterate_to_end_of_row();
            break;
        case CTRL_L:
            center_screen_upon_cursor();
            break;
        case CTRL_SLASH:
            set_status("Undo not implemented yet");
            break;
        case CTRL_META_SPACE:
            set_mark_to_cursor();
            find_accompanying_brace(ARROW_RIGHT);
            break;
        case CTRL_U:
            obliterate_to_start_of_row();
            break;
        case CTRL_Y:
            set_status("Paste from clipboard not implemented yet.");
            break;
        case CTRL_Z:
            set_status("Backgrounding editor not implemented yet.");
            break;
        case META_CTRL_P:
            find_accompanying_brace(ARROW_LEFT);
            break;
        case META_CTRL_N:
            find_accompanying_brace(ARROW_RIGHT);
            break;
        case ESC:
            break;
        case META_OPEN_BRACE:
            editor_insert_character('{');
            editor_insert_character('}');
            move_cursor(ARROW_LEFT);
            break;
        case META_OPEN_BRACKET:
            editor_insert_character('[');
            editor_insert_character(']');
            move_cursor(ARROW_LEFT);
            break;
        case META_OPEN_PAREN:
            editor_insert_character('(');
            editor_insert_character(')');
            move_cursor(ARROW_LEFT);
            break;
        case META_R:
            set_status("Insert raw character: ");
            c = read_keypresses_from_raw_terminal(fd);
            set_status("Insert raw character: %c", c);
        case META_DOT:
            scroll_screen(+1);
            break;
        case META_COMMA:
            scroll_screen(-1);
            break;
        default:
            editor_insert_character(c);
            break;
    }

    quit_times = NTH_QUIT_TIMES; /* Reset it to the original value. */
}

void initialize_editor(int tabWidth) {
    edState.cursor.x = 0;
    edState.cursor.y = 0;
    edState.scrPos.y = 0;
    edState.scrPos.x = 0;
    edState.tabWidth = tabWidth;
    edState.numrows = 0;
    edState.row = NULL;
    edState.dirty = 0;
    edState.filename = NULL;
    edState.inputCommand = ab_init(32);
    edState.syntax = NULL;
    clear_mark();
    if (get_window_size(STDIN_FILENO,STDOUT_FILENO,&edState.scrSize.y,&edState.scrSize.x) == -1) {
        perror("Unable to query the screen for size (columns / rows)");
        exit(1);
    }
    edState.scrSize.y -= STATUS_BAR_COUNT;
}

int show_help(char *proggy) {
    fprintf(stderr, "Usage: %s [-t n] <filename>\n", proggy);
    fprintf(stderr, "\t-t n\tSets tab width to n spaces\n");
    return 1;
}

int main_editor_loop(void) {
    refresh_screen();
    ui_process_keypress(STDIN_FILENO);
    return 1;
}

int startup_editor(char *filePath, int tabWidth) {
    initialize_editor(tabWidth);
    syntax_color_scheme(filePath);
    open_file(filePath);
    enable_raw_term_mode(STDIN_FILENO);
    set_status("HELP: F2 = save | Ctrl-Q = quit | Ctrl-S = Search");
    while(main_editor_loop());
    return 0;    
}

int switch_value_index(const char *cmdSwitch, int argc, char **argv) {
    for(--argc;argc>0;--argc)
        if(strcmp(argv[argc], cmdSwitch)==0)
            return argc+1;
    return -1;
}

int parse_tab_width(int argc, char **argv) {
    int tabArg=switch_value_index("-t", argc, argv);
    int parsed;
    if(tabArg<0 || tabArg>=argc)
        return NTH_DEFAULT_TAB_WIDTH;
    parsed=parse_number(argv[tabArg]);
    if(parsed<1 || parsed>NTH_MAX_TAB_WIDTH)
        return NTH_DEFAULT_TAB_WIDTH;
    return parsed;
}

int main(int argc, char **argv) {
    int tabArg=switch_value_index("-t", argc, argv);
    int filePos=argc-1;
    if (argc < 2 || tabArg>=filePos)
        return show_help(argv[0]);
    return startup_editor(argv[filePos],
                          parse_tab_width(argc, argv));
}
