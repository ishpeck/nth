all: nth-editor

nth-editor: nth.c nth.h highlighting.h abuf.h abuf.c Makefile
	$(CC) -o nth-editor abuf.c nth.c -Wall -W -pedantic -std=c99

clean:
	rm nth

nth: nth.c nth.h highlighting.h Makefile
	$(CC) -g -o nth-editor-debug nth.c -Wall -W -pedantic -std=c99

