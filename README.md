# nth

A text editor originally forked from Kilo.

Usage: `nth <filename>`

Where K is a reasonable number of editors and X is an integer greater
than zero, N=K+X.  This is the Nth editor to exist.

## Goals

The objective of this project is to...

 - Make a terminal editor
 - Without modes (because vi fills that need)
 - That's lighter than emacs (which should be easy to do on accident)
 - That's for adults (because nano exists as an alternative to that)
 - That can be used to edit the code that compiles to it
 - That doesn't have dependencies up the wazoo (preferably none)

## Roadmap

 - RegEx search 
 - Text and regex replace 
 - Launch child processes
 - Launching child processes
 - Sending regions as input for child processes
 - Inserting output of child processes into your editing view
 - Replacing regions with output of child process
 - Customizable Keys

# Key Bindings

    F2: Save

    Cursor keys: Do what you'd expect

    CTRL+Space: Set mark (begin selecting) 
    CTRL+A: Jump to beginning of line
    CTRL+B: Move cursor back
    CTRL+D: Delete character
    CTRL+E: Jump to end of line
    CTRL+F: Move cursor forward
    CTRL+G: "Get rid of" mark (deselect)
    CTRL+H: Rub-out (same as backspace)
    CTRL+I: Indent current line
    CTRL+K: Delete everything from cursor to EOL
    CTRL+L: Center screen on cursor
    CTRL+M: New line
    CTRL+N: Move cursor down (next line)
    CTRL+O: Insert line after cursor
    CTRL+P: Move cursor up (to previous line)
    CTRL+Q: Quit
    CTRL+R: Reverse search
    CTRL+S: Search forward
    CTRL+T: Transpose characters
    CTRL+U: Delete everything on line up to cursor
    CTRL+V: Move cursor down one screen's height
    CTRL+W: Backspace whole word

    ALT+[: Insert a bracket pair []
    ALT+(: Insert parens ()
    ALT+{: Insert a brace pair {}
    ALT+<: Jump to top of document
    ALT+>: Jump to bottom of document
    ALT+B: Jump backword by word
    ALT+F: Jump forword by word
    ALT+R: Insert raw content

We're deliberately avoiding binding CTRL+X so you can reserve that as your
"prefix" key within `screen` or `tmux`.

# Kilo
Kilo is a small text editor in less than 1K lines of code (counted with cloc).

A screencast is available here: https://asciinema.org/a/90r2i9bq8po03nazhqtsifksb

Kilo does not depend on any library (not even curses). It uses fairly standard
VT100 (and similar terminals) escape sequences. The project is in alpha
stage and was written in just a few hours taking code from my other two
projects, load81 and linenoise.

People are encouraged to use it as a starting point to write other editors
or command line interfaces that are more advanced than the usual REPL
style CLI.

Kilo was written by Salvatore Sanfilippo aka antirez and is released
under the BSD 2 clause license.
